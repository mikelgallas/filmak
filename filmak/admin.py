from filmak.models import Filma, Bozkatzailea
from django.contrib import admin


class BozkatzaileaInline(admin.StackedInline):
	model = Bozkatzailea
	extra = 3

class FilmaAdmin(admin.ModelAdmin):
	fieldsets = [
		(None, {'fields': ['izenburua']}),
		('Zuzendaria', {'fields': ['zuzendaria']}),
		('Urtea', {'fields': ['urtea']}),
		('Generoa', {'fields': ['generoa']}),
		('Sinopsia', {'fields': ['sinopsia']}),
		('Bozkak', {'fields': ['bozkak']}),
	]

admin.site.register(Filma, FilmaAdmin)
admin.site.register(Bozkatzailea)
