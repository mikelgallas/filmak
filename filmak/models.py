from django.db import models
from django.contrib.auth.models import User

# Create your models here.

GENERO_AUKERAK = (
        ('TH', 'thrillerra'),
        ('DR', 'drama'),
        ('ER', 'erromantikoa'),
        ('MI', 'misterioa'),
        ('KO', 'komedia'),
        ('DO', 'dokumentala'),
)

class Filma(models.Model):
        izenburua = models.CharField(max_length=100)
        zuzendaria = models.CharField(max_length=60)
        urtea = models.IntegerField()
        generoa = models.CharField(max_length=2)
        sinopsia = models.CharField(max_length=500)
        bozkak = models.IntegerField()
	def __unicode__(self):
		return self.izenburua

class Bozkatzailea(models.Model):
	gogoko_filmak = models.ManyToManyField(Filma)
	erabiltzaile = models.OneToOneField(User)
	def __unicode__(self):
		return self.erabiltzaile.username
	def bozkatuDu(self,film_id):
		for f in self.gogoko_filmak.all():
			if f.id == film_id:
				return True
		return False
