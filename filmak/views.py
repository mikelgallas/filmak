# Create your views here.
from django.http import HttpResponse, HttpResponseRedirect
from filmak.models import Filma, Bozkatzailea
from django.template import *
from django.shortcuts import render, render_to_response, get_object_or_404
from django.contrib.auth.forms import UserCreationForm, AuthenticationForm
from django.contrib.auth import authenticate, login, logout
from django.contrib.auth.models import User
from django.core.paginator import Paginator, EmptyPage, PageNotAnInteger
from django.contrib.auth.decorators import login_required

def index(request):
	if not request.user.is_authenticated():
		return render_to_response('filmak/index.html')
	else:
		return render(request, 'filmak/sarrera.html')

def sartu(request):
	if not request.user.is_authenticated():

		if request.method == 'POST':
			erab = request.POST['erabiltzailea']
			pasa = request.POST['password']
			user = authenticate(username=erab, password=pasa)
			if user is not None:
				if user.is_active:
					login(request, user)
					#Berbideratu login ondorengo orri batera
					return HttpResponseRedirect('/filmak/sarrera')
				else:
					#Errore-mezua: 'kontua desgaituta'
					return render(request, 'filmak/sartu.html',{'mezua':"Kontua desgaituta dago!"})
			else:
				#Errore-mezua: 'login desegokia'
				return render(request, 'filmak/sartu.html',{'mezua':"Erabiltzaile izen eta pasahitz konbinaketa gaizki dago. Saiatu berriro."})
		else:
			return render(request, 'filmak/sartu.html')
	else:
		return render(request, 'filmak/sarrera.html')


def sartu2(request):
	if not request.user.is_authenticated():

		if request.method == 'POST':
			form = AuthenticationForm(data=request.POST)
			if form.is_valid():
				#log in the user
				return HttpResponseRedirect('/filmak/sarrera')
		else:
			form = AuthenticationForm()
			return render(request,'filmak/sartu2.html',{'form':form})
	else:
		return render(request, 'filmak/sarrera.html')

def erregistratu(request):
	if not request.user.is_authenticated():

		if request.method == 'POST':
			erab = request.POST['erabiltzailea']
			if erab=="":
				return render(request, 'filmak/erregistratu.html',{'mezua':"Erabiltzaile izena ezin da hutsa izan."})
			if not User.objects.filter(username=erab).exists():
				pass1 = request.POST['password']
				pass2 = request.POST['password2']
				if pass1 == pass2:
					user=User(username=erab)
					user.save()
					user.set_password(pass1)
					user.save()
					return HttpResponseRedirect('/filmak/sarrera')
				else:
					return render(request, 'filmak/erregistratu.html',{'mezua':"Pasahitzak desberdinak dira."})
			else:
				return render(request, 'filmak/erregistratu.html', {'mezua':"Erabiltzailea jada existitzen da. Erabili beste erabiltzaile izen bat."})
		else:
			return render(request,'filmak/erregistratu.html')
	else:
		return render(request, 'filmak/sarrera.html')

def erregistratu2(request):
	if not request.user.is_authenticated():
		if request.method == 'POST':
			form = UserCreationForm(request.POST)
			if form.is_valid():
				form.save()
				#log the user in
				return HttpResponseRedirect('/filmak/sarrera')
		else:
			form = UserCreationForm()
	
		return render(request,'filmak/erregistratu2.html',{'form':form})
	else:
		return render(request,'filmak/sarrera.html')

def logout_bista(request):
	logout(request)
	return HttpResponseRedirect('/filmak/index')

@login_required(login_url='/filmak/sartu')
def sarrera(request):
	filmen_lista = Filma.objects.all().order_by('id')
	user = request.user.username
	return render(request,'filmak/sarrera.html',{'filmen_lista':filmen_lista, 'user':user})

@login_required(login_url='/filmak/sartu')
def detail(request, film_id):
	f = get_object_or_404(Filma, pk=film_id)
	if request.method == 'POST':
		return HttpResponseRedirect('/filmak/vote/'+film_id)
	else:
		return render(request,'filmak/detail.html',{'filma': f})

@login_required(login_url='/filmak/sartu')
def listing(request):
	filmen_lista = Filma.objects.all()
	paginator = Paginator(filmen_lista, 2)

	page = request.GET.get('page')
	try:
		filmak = paginator.page(page)
	except PageNotAnInteger:
		# Orrialdea zenbaki bat ez bada joan lehen orrira
		filmak = paginator.page(1)
	except EmptyPage:
		# Orrialdea handiegia bada azken orrira joan
		filmak = paginator.page(paginator.num_pages)

	return render_to_response('filmak/list.html', {'filmak': filmak})

@login_required(login_url='/filmak/sartu')
def vote(request, film_id):
	f = get_object_or_404(Filma, pk=film_id)
	if Bozkatzailea.objects.filter(erabiltzaile=request.user).exists():
		b = Bozkatzailea.objects.get(erabiltzaile=request.user)

		if b.bozkatuDu(f.id):
			mezua = "filma bozkatu duzu jada"
		else:
			f.bozkak = f.bozkak + 1
			f.save()
                	b.gogoko_filmak.add(f)
			b.save()
			mezua = "filma ondo bozkatu da"
	else:
		b = Bozkatzailea(erabiltzaile=request.user)
		b.save()
		f.bozkak = f.bozkak + 1
		f.save()
		b.gogoko_filmak.add(f)
		b.save()
		mezua = "filma ondo bozkatu da"
	return render(request, 'filmak/vote.html', {'mezua': mezua, 'f': f})

@login_required(login_url='/fimak/sartu')
def bozkatzaileak(request, film_id):
	f = get_object_or_404(Filma, pk=film_id)
	bozkatzaileak = []
	for b in Bozkatzailea.objects.all():
		if b.bozkatuDu(f.id):
			bozkatzaileak.append(b)
	return render_to_response('filmak/bozkatzaileak.html', {'filma':f, 'bozkatzaileak': bozkatzaileak})
