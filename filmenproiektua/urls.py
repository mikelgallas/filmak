from django.conf.urls import patterns, include, url
from django.contrib.auth.views import logout

# Uncomment the next two lines to enable the admin:
from django.contrib import admin
admin.autodiscover()

urlpatterns = patterns('',
    # Bistak:
    url(r'^filmak/$', 'filmak.views.index', name='index'),
    url(r'^filmak/index$', 'filmak.views.index', name='index'),
    url(r'^filmak/sartu$', 'filmak.views.sartu', name='sartu'),
    url(r'^filmak/sartu2$', 'filmak.views.sartu2', name='sartu2'),
    url(r'^filmak/erregistratu$', 'filmak.views.erregistratu', name='erregistratu'),
    url(r'^filmak/erregistratu2$', 'filmak.views.erregistratu2', name='erregistratu2'),
    url(r'^filmak/logout$', 'filmak.views.logout_bista', name='logout'),
    url(r'^filmak/logout2$', logout, {'next_page': '/filmak/index'} , name='logout2'),
    url(r'^filmak/sarrera$', 'filmak.views.sarrera', name='sarrera'),

    url(r'^filmak/(?P<film_id>\d+)/$', 'filmak.views.detail'),
    url(r'^filmak/list$', 'filmak.views.listing', name='listing'),

    url(r'^filmak/vote/(?P<film_id>\d+)/$', 'filmak.views.vote'),
    url(r'^filmak/bozkatzaileak/(?P<film_id>\d+)/$', 'filmak.views.bozkatzaileak'),
    # url(r'^filmenproiektua/', include('filmenproiektua.foo.urls')),

    # Uncomment the admin/doc line below to enable admin documentation:
    # url(r'^admin/doc/', include('django.contrib.admindocs.urls')),

    # Uncomment the next line to enable the admin:
    url(r'^admin/', include(admin.site.urls)),
)
